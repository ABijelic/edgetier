package hr.fer.rassus.labos.edgeTier.Model.DataTransferObject;

import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Random;

public class EdgeRegisterRequest {

    private String ip;

    private int port;

    private Double latitude;

    private Double longitude;

    public EdgeRegisterRequest(int port) throws SocketException, UnknownHostException {
//        try(final DatagramSocket socket = new DatagramSocket()){
//            socket.connect(InetAddress.getByName("8.8.8.8"), 10002);
//            ip = socket.getLocalAddress().getHostAddress();
//        }
        this.ip = "localhost";
        this.port = port;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public String getIp() {
        return ip;
    }

    public int getPort() {
        return port;
    }

    public Double getLatitude() {
        return latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    @Override
    public String toString() {
        return "EdgeRegisterRequest{" +
                "ip='" + ip + '\'' +
                ", port=" + port +
                ", latitude=" + latitude +
                ", longitude=" + longitude +
                '}';
    }
}
