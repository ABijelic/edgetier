package hr.fer.rassus.labos.edgeTier.Model.DataTransferObject;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

public class MeasurementForCentralRepository {

    Double temperature;
    Double pressure;
    Double noise;
    String measurerId;

    @JsonFormat(pattern="yyyy-MM-ddTHH:mm:ss")
    Date measurerTime;
    Location location;


    public Double getTemperature() {
        return temperature;
    }

    public void setTemperature(Double temperature) {
        this.temperature = temperature;
    }

    public Double getPressure() {
        return pressure;
    }

    public void setPressure(Double pressure) {
        this.pressure = pressure;
    }

    public Double getNoise() {
        return noise;
    }

    public void setNoise(Double noise) {
        this.noise = noise;
    }

    public String getMeasurerId() {
        return measurerId;
    }

    public void setMeasurerId(String measurerId) {
        this.measurerId = measurerId;
    }


    @JsonFormat(pattern="yyyy-MM-ddTHH:mm:ss")
    public Date getMeasurerTime() {
        return measurerTime;
    }

    public void setMeasurerTime(Date measurerTime) {
        this.measurerTime = measurerTime;
    }


    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }
}
