package hr.fer.rassus.labos.edgeTier.Controllers;

import com.bbn.openmap.proj.coords.LatLonPoint;
import com.bbn.openmap.proj.coords.MGRSPoint;
import hr.fer.rassus.labos.edgeTier.EdgeTierApplication;
import hr.fer.rassus.labos.edgeTier.Model.DataTransferObject.BloomFilterDTO;
import hr.fer.rassus.labos.edgeTier.Model.DataTransferObject.Location;
import hr.fer.rassus.labos.edgeTier.Model.DataTransferObject.MeasurementClientRequest;
import hr.fer.rassus.labos.edgeTier.Service.BloomFilterService;
import hr.fer.rassus.labos.edgeTier.Service.CommunicationService;
import hr.fer.rassus.labos.edgeTier.Service.MeasurementService;
import lombok.extern.slf4j.Slf4j;
import org.apache.hadoop.util.bloom.BloomFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.awt.geom.Point2D;

@Slf4j
@RestController
public class MeasurementController {

    private MeasurementService measurementService;

    private BloomFilterService bloomFilterService;

    private CommunicationService communicationService;

    @Autowired
    public MeasurementController(MeasurementService measurementService, BloomFilterService bloomFilterService,
                                 CommunicationService communicationService) {
        this.measurementService = measurementService;
        this.bloomFilterService = bloomFilterService;
        this.communicationService = communicationService;
    }

    @PostMapping("/api/measurement")
    public ResponseEntity addMeasurement(@RequestBody MeasurementClientRequest measurementRequest) {
        boolean success = measurementService.addMeasurement(measurementRequest.toMeasurement());
        communicationService.saveMessageREST(EdgeTierApplication.BLOOM_FILTER, false, measurementRequest.getUsername(), "POST", measurementRequest.toString());
        if(!success && EdgeTierApplication.BLOOM_FILTER) {
            BloomFilterDTO bloomFilter = BloomFilterDTO.fromBloomFileter(bloomFilterService.getBloomFilter(new MGRSPoint(measurementRequest.getMgrs())), bloomFilterService.getNbHash());
            communicationService.saveMessageREST(EdgeTierApplication.BLOOM_FILTER, true, measurementRequest.getUsername(), "POST", bloomFilter.toString());
            return new ResponseEntity(bloomFilter, HttpStatus.OK);
        }
        communicationService.saveMessageREST(EdgeTierApplication.BLOOM_FILTER, true, measurementRequest.getUsername(), "POST", Boolean.toString(success));
        return new ResponseEntity<>(success, HttpStatus.OK);
    }


    @GetMapping("/api/bloom-filter/{mgrs}")
    public ResponseEntity getBloomFilter(@PathVariable String mgrs, HttpServletRequest httpServletRequest) {
        String username = httpServletRequest.getHeader("username");
        communicationService.saveMessageREST(EdgeTierApplication.BLOOM_FILTER, false, username, "GET", mgrs);
        if (!EdgeTierApplication.BLOOM_FILTER) {
            log.warn("edge is not in bloom filter mode and it arrive request for bloom filter");
            return new ResponseEntity<>("Edge is not in bloom filter mode", HttpStatus.BAD_REQUEST);
        }
        MGRSPoint mgrsPoint = new MGRSPoint(mgrs);
        mgrsPoint.setAccuracy(MGRSPoint.ACCURACY_1_METER);
        BloomFilterDTO bloomFilter = BloomFilterDTO.fromBloomFileter(bloomFilterService.getBloomFilter(mgrsPoint), bloomFilterService.getNbHash());
        bloomFilter.setData(null);
        communicationService.saveMessageREST(EdgeTierApplication.BLOOM_FILTER, true, username, "GET", bloomFilter.toString());
        return new ResponseEntity(bloomFilter, HttpStatus.OK);
    }
}
