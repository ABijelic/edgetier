package hr.fer.rassus.labos.edgeTier.Controllers;

import hr.fer.rassus.labos.edgeTier.Model.DataTransferObject.MeasurerResponse;
import hr.fer.rassus.labos.edgeTier.Service.MeasurementService;
import hr.fer.rassus.labos.edgeTier.Service.MeasurerService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;



@RestController
@CrossOrigin
@Slf4j
public class RegisterMeasurersController {

    private MeasurerService measurerService;

    private MeasurementService measurementService;

    @Autowired
    public RegisterMeasurersController(MeasurerService measurerService, MeasurementService measurementService) {
        this.measurerService = measurerService;
        this.measurementService = measurementService;
    }


    @RequestMapping(value = "/api/measurer/unregister", method = RequestMethod.PUT)
    public ResponseEntity unregisterMeasurer(@RequestBody MeasurerResponse measurer) {
        log.info("delete measurer {}", measurer.toMeasurer());
        if (measurerService.removeMeasurer(measurer.toMeasurer())) {
            return new ResponseEntity<>(true, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(false, HttpStatus.OK);
        }
    }

    @RequestMapping(value = "/api/measurer", method = RequestMethod.POST)
    public ResponseEntity registerMeasurer(@RequestBody MeasurerResponse measurer) {
        log.info("add measurer {}", measurer);
        MeasurerResponse response = new MeasurerResponse();
        response.setId(measurer.getId());
        response.setUsername(measurer.getUsername());
        if (measurerService.addMeasurer(measurer.toMeasurer())) {
            return new ResponseEntity<>(true, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(false, HttpStatus.OK);
        }
    }
}
