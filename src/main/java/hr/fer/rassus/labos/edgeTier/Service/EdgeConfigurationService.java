package hr.fer.rassus.labos.edgeTier.Service;

import com.bbn.openmap.proj.coords.MGRSPoint;
import hr.fer.rassus.labos.edgeTier.Model.Measurer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class EdgeConfigurationService {

    private String username;

    private MGRSPoint mgrs;

    private double latitude;

    private double longitude;

    private String address;

    private List<String> areas;


    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public MGRSPoint getMgrs() {
        return mgrs;
    }

    public void setMgrs(String mgrs) {
        this.mgrs = new MGRSPoint(mgrs);
        this.areas = new ArrayList<>();

    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

}
