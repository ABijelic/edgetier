package hr.fer.rassus.labos.edgeTier.Model;

import hr.fer.rassus.labos.edgeTier.Model.DataTransferObject.Location;
import hr.fer.rassus.labos.edgeTier.Model.DataTransferObject.MeasurementForCentralRepository;

import java.util.Date;

public class Measurement {

    private double pressure;
    private double temperature;
    private double noise;
    private Date timestamp;
    private Location location;
    private String username;
    private String mgrs;

    public Measurement() {
        timestamp = new Date();
    }



    public String getMgrs() {
        return mgrs;
    }

    public void setMgrs(String mgrs) {
        this.mgrs = mgrs;
    }

    public double getNoise() {
        return noise;
    }

    public void setNoise(double noise) {
        this.noise = noise;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public double getPressure() {
        return pressure;
    }

    public void setPressure(double pressure) {
        this.pressure = pressure;
    }

    public double getTemperature() {
        return temperature;
    }

    public void setTemperature(double temperature) {
        this.temperature = temperature;
    }

    public MeasurementForCentralRepository toMeasurementForCentralRepository() {
        MeasurementForCentralRepository measurementForCentralRepository =
                new MeasurementForCentralRepository();
        measurementForCentralRepository.setLocation(location);
        measurementForCentralRepository.setMeasurerId(username);
        measurementForCentralRepository.setMeasurerTime(timestamp);
        measurementForCentralRepository.setNoise(noise);
        measurementForCentralRepository.setPressure(pressure);
        measurementForCentralRepository.setTemperature(temperature);
        return measurementForCentralRepository;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public String toString() {
        return "Measurement{" +
                "pressure=" + pressure +
                ", temperature=" + temperature +
                ", timestamp=" + timestamp +
                ", mgrs=" + mgrs +
                '}';
    }


}
