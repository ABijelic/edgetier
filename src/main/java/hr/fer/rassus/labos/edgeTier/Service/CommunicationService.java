package hr.fer.rassus.labos.edgeTier.Service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.StringJoiner;

@Service
@Slf4j
public class CommunicationService {
    public static final String BLOOM_FILTER = "BloomFilter";
    public static final String TOP_K = "TopK";

    public static final String TCP = "TCP";
    public static final String REST = "REST";


    @Autowired
    private EdgeConfigurationService edgeConfigurationService;

    private BufferedWriter bufferedWriter;

    private String username;


    public boolean saveMessageREST(boolean bloomFilter, boolean sendFromEdge, String measurerUsername, String method, String message) {
        BufferedWriter bufferedWriter = getBufferedWriter();
        if (bufferedWriter == null) {
            return false;
        }

        try {
            bufferedWriter.write(generateLine(bloomFilter,
                    sendFromEdge, measurerUsername, message, REST+"-"+method));
            bufferedWriter.newLine();
            return true;
        } catch (IOException e) {
            return false;
        }

    }

    public boolean saveMessageTCP(boolean bloomFilter, boolean sendFromEdge, String measurerUsername, String message) {
        BufferedWriter bufferedWriter = getBufferedWriter();
        if (bufferedWriter == null) {
            return false;
        }

        try {
            bufferedWriter.write(generateLine(bloomFilter,
                    sendFromEdge, measurerUsername, message, TCP));
            bufferedWriter.newLine();
            return true;
        } catch (IOException e) {
            return false;
        }
    }

    private String generateLine(boolean bloomFilter, boolean sendFromEdge, String measurerUsername, String message, String protocol) {
        StringJoiner stringBuilder = new StringJoiner(";");

        stringBuilder.add(bloomFilter ? BLOOM_FILTER : TOP_K);
        stringBuilder.add(sendFromEdge ? username : measurerUsername);
        stringBuilder.add(sendFromEdge ? measurerUsername : username);
        stringBuilder.add(protocol);
        stringBuilder.add(message);

        return stringBuilder.toString();
    }

    public void closeWriter() {
        try {
            bufferedWriter.close();
        } catch (IOException e) {
        }
    }


    private BufferedWriter getBufferedWriter() {
        if (bufferedWriter != null) {
            return bufferedWriter;
        }

        username = edgeConfigurationService.getUsername();
        if (username == null) {
            return null;
        }

        try {
            bufferedWriter = Files.newBufferedWriter(Paths.get(username + "-communications.txt"));
            String beginLine = "algoritam;pošiljatelj;primatelj;načinslanja;poruka";
            bufferedWriter.write(beginLine);
            bufferedWriter.newLine();
            return bufferedWriter;
        } catch (IOException e) {
            return null;
        }
    }
}
