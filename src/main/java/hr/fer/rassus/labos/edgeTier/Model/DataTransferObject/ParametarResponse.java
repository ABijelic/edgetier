package hr.fer.rassus.labos.edgeTier.Model.DataTransferObject;

public class ParametarResponse {
    private String username;
    private int parametar;

    public ParametarResponse() {
    }

    public static ParametarResponse parseToParametarResponse(String string) {
        ParametarResponse parametarResponse = new ParametarResponse();
        String[] data = string.split(";");
        for( String parametar: data) {
            String[] keyValue = parametar.split(":");
            switch (keyValue[0]) {
                case "username":
                    parametarResponse.username = keyValue[1];
                    break;
                case "parameter":
                    parametarResponse.parametar = Integer.parseInt(keyValue[1]);
                    break;
            }
        }
        return parametarResponse;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getParametar() {
        return parametar;
    }

    public void setParametar(int parametar) {
        this.parametar = parametar;
    }
}
