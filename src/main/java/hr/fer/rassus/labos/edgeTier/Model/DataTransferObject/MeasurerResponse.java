package hr.fer.rassus.labos.edgeTier.Model.DataTransferObject;

import hr.fer.rassus.labos.edgeTier.Model.Address;
import hr.fer.rassus.labos.edgeTier.Model.Measurer;

public class MeasurerResponse {

    private String username;
    private String id;
    private Location location;
    private Address address;

    public MeasurerResponse() {
    }

    public Measurer toMeasurer() {
        return new Measurer(this);
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return "MeasurerResponse{" +
                "username='" + username + '\'' +
                ", id='" + id + '\'' +
                ", location=" + location +
                ", address=" + address +
                '}';
    }
}
