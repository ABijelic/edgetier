package hr.fer.rassus.labos.edgeTier.Model.DataTransferObject;

import java.util.List;

public class MeasurementsForCentralRepository {
    private List<MeasurementForCentralRepository> measurements;


    public MeasurementsForCentralRepository(List<MeasurementForCentralRepository> measurements) {
        this.measurements = measurements;
    }

    public List<MeasurementForCentralRepository> getMeasurements() {
        return measurements;
    }

    public void setMeasurements(List<MeasurementForCentralRepository> measurements) {
        this.measurements = measurements;
    }
}
