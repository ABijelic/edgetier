package hr.fer.rassus.labos.edgeTier.Service;


import com.bbn.openmap.proj.coords.MGRSPoint;
import hr.fer.rassus.labos.edgeTier.EdgeTierApplication;
import hr.fer.rassus.labos.edgeTier.Model.DataTransferObject.BloomFilterDTO;
import hr.fer.rassus.labos.edgeTier.Model.Measurement;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.output.ByteArrayOutputStream;
import org.apache.hadoop.util.bloom.BloomFilter;
import org.apache.hadoop.util.bloom.Key;
import org.apache.hadoop.util.hash.Hash;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.awt.*;
import java.awt.geom.Point2D;
import java.io.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Slf4j
@Service
public class BloomFilterService {
    public static final int NUMBER_OF_UNDER_AREAS = 100;
    private Map<String, BloomFilter> bloomFilters;
    private int vectorSize;
    private int nbHash;

    @Autowired
    public BloomFilterService() {
        this.bloomFilters = new ConcurrentHashMap<>();
        this.vectorSize = getOptimalBloomFilterSize(NUMBER_OF_UNDER_AREAS, EdgeTierApplication.FALSE_POS_RATE);
        this.nbHash = getOptimalK(NUMBER_OF_UNDER_AREAS, vectorSize);
    }

    public BloomFilter getBloomFilter(MGRSPoint mgrsPoint) {
        mgrsPoint.setAccuracy(MGRSPoint.ACCURACY_100_METER);
        log.info("get bloomfilter for {}", mgrsPoint.getMGRS());
        if(bloomFilters.containsKey(mgrsPoint.getMGRS())) {
            log.info("bloomfilter already exist");
            return bloomFilters.get(mgrsPoint.getMGRS());
        }
        log.info("create new bloomfilter");


        BloomFilter bloomFilter = new BloomFilter(this.vectorSize, this.nbHash, Hash.MURMUR_HASH);

        bloomFilters.put(mgrsPoint.getMGRS(), bloomFilter);
        return bloomFilter;
    }

    public void initializBloomFilters() {
        bloomFilters.forEach((mgrs, bloomFilter)-> {
            bloomFilters.put(mgrs, new BloomFilter(this.vectorSize, this.nbHash, Hash.MURMUR_HASH));
        });
    }

    public boolean addMeasurement(Measurement measurement) {
        if (isValidMeasurement(measurement)) {
            return false;
        }
        MGRSPoint mgrsPoint = new MGRSPoint(measurement.getMgrs());
        mgrsPoint.setAccuracy(MGRSPoint.ACCURACY_100_METER);
        BloomFilter bloomFilter = getBloomFilter(mgrsPoint);
        String key = mgrsPoint.getMGRS()+measurement.getTemperature();
        bloomFilter.add(new Key(key.getBytes()));
        return true;
    }

    private boolean isValidMeasurement(Measurement measurement) {
        MGRSPoint mgrsPoint = new MGRSPoint(measurement.getMgrs());
        mgrsPoint.setAccuracy(MGRSPoint.ACCURACY_100_METER);
        BloomFilter bloomFilter = getBloomFilter(mgrsPoint);
        String key = mgrsPoint.getMGRS()+measurement.getTemperature();
        return bloomFilter.membershipTest(new Key(key.getBytes()));
    }

    private static int getOptimalBloomFilterSize(int numRecords, double falsePosRate) {
        int size = (int) (-numRecords * (float) Math.log(falsePosRate) / Math
                .pow(Math.log(2), 2));
        return size;
    }

    private static int getOptimalK(float numMembers, float vectorSize) {
        return (int) Math.round(vectorSize / numMembers * Math.log(2));
    }

    public int getNbHash() {
        return nbHash;
    }
}
