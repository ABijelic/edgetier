package hr.fer.rassus.labos.edgeTier.Model.DataTransferObject;

import hr.fer.rassus.labos.edgeTier.Model.Measurement;

public class MeasurementClientRequest {
    private String username;
    private Double pressure;
    private Double temperature;
    private Double noise;
    private Location location;
    private String mgrs;

    public MeasurementClientRequest() {
    }

    public Measurement toMeasurement() {
        Measurement measurement = new Measurement();
        measurement.setPressure(pressure);
        measurement.setTemperature(temperature);
        measurement.setLocation(location);
        measurement.setMgrs(mgrs);
        measurement.setNoise(noise);
        measurement.setUsername(username);
        return measurement;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Double getPressure() {
        return pressure;
    }

    public void setPressure(Double pressure) {
        this.pressure = pressure;
    }

    public Double getTemperature() {
        return temperature;
    }

    public void setTemperature(Double temperature) {
        this.temperature = temperature;
    }

    public Double getNoise() {
        return noise;
    }

    public void setNoise(Double noise) {
        this.noise = noise;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public String getMgrs() {
        return mgrs;
    }

    public void setMgrs(String mgrs) {
        this.mgrs = mgrs;
    }

    @Override
    public String toString() {
        return "MeasurementClientRequest{" +
                "username='" + username + '\'' +
                ", pressure=" + pressure +
                ", temperature=" + temperature +
                ", noise=" + noise +
                ", location=" + location +
                ", mgrs='" + mgrs + '\'' +
                '}';
    }
}
