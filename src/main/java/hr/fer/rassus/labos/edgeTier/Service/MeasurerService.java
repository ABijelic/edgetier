package hr.fer.rassus.labos.edgeTier.Service;

import hr.fer.rassus.labos.edgeTier.EdgeTierApplication;
import hr.fer.rassus.labos.edgeTier.Model.DataTransferObject.ParametarResponse;
import hr.fer.rassus.labos.edgeTier.Model.Measurer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.*;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
public class MeasurerService {

    private List<Measurer> measurers;

    private List<Measurer> topKMeasures;

    private CommunicationService communicationService;

    @Autowired
    public MeasurerService(CommunicationService communicationService) {
        this.communicationService = communicationService;
        measurers = new ArrayList<>();
    }

    public List<Measurer> getAllMeasurers() {
        return measurers;
    }

    public boolean addMeasurer(Measurer measurer) {
       return  measurers.add(measurer);
    }

    public boolean removeMeasurer(Measurer measurer) {
        return measurers.remove(measurer);
    }

    public boolean addMeasurers(List<Measurer> newMeasurers) {
        return measurers.addAll(newMeasurers);
    }

    public List<Measurer> calculateTopK(int topk) {
        topKMeasures = measurers.stream().sorted((measurer1, measurer2) -> Integer.compare(measurer1.getParametar(), measurer2.getParametar())).limit(topk).collect(Collectors.toList());
        return topKMeasures;
    }

    public List<Measurer> getTopKMeasures() {
        return topKMeasures;
    }



    public void sendRequestsForParametar(String message) {
        measurers.forEach(measurer -> {
            Socket socket = sendMessageOnMeasurer(measurer,message);

            if (socket != null) {
                BufferedReader inFromServer = null;
                try {
                    inFromServer = new BufferedReader(new InputStreamReader(
                            socket.getInputStream()));
                    String p = inFromServer.readLine();
                    communicationService.saveMessageTCP(EdgeTierApplication.BLOOM_FILTER, false, measurer.getUsername(), p);
                    ParametarResponse parametar = ParametarResponse.parseToParametarResponse(p);
                    if (parametar.getUsername() != measurer.getUsername()) {
                        System.out.println("username of message is not valid!");
                    }
                    measurer.setParametar(parametar.getParametar());

                    socket.close();
                } catch (IOException e) {
                }

            } else {
                measurer.setParametar(0);
            }

        });
    }


    public void sendOnAllMeasurement(String message) {
        measurers.forEach(measurer -> {
            Socket socket = sendMessageOnMeasurer(measurer,message);
            try {
                socket.close();
            } catch (IOException e) {
            }
        });
    }

    public void sendMessageOnTopKMeasurement(String messageGetMeasurementTopK) {
        topKMeasures.forEach(measurer -> {
            try {
                sendMessageOnMeasurer(measurer, messageGetMeasurementTopK).close();
            } catch (Exception e) {
            }
        });
    }

    private Socket sendMessageOnMeasurer(Measurer measurer, String message) {
        try {
            Socket socket = new Socket(measurer.getAddress(), measurer.getPort());
            socket.setSoTimeout(1000);
            PrintWriter outToServer = new PrintWriter(new OutputStreamWriter(
                    socket.getOutputStream()), true);
            communicationService.saveMessageTCP(EdgeTierApplication.BLOOM_FILTER, true, measurer.getUsername(), message);
            outToServer.println(message);
            outToServer.flush();
            return socket;
        } catch (IOException e) {
            log.warn(e.toString());
            return null;
        }

    }
}
