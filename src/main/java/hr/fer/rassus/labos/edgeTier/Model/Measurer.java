package hr.fer.rassus.labos.edgeTier.Model;

import hr.fer.rassus.labos.edgeTier.Model.DataTransferObject.MeasurerResponse;

import java.util.Objects;

public class Measurer {
    private String id;
    private String username;
    private String address;
    private int port;
    private int parametar;

    public Measurer() {
    }

    public Measurer(String id, String username, String address, int port) {
        this.id = id;
        this.username = username;
        this.address = address;
        this.port = port;
    }

    public Measurer(MeasurerResponse measurerResponse) {
        this(measurerResponse.getId(), measurerResponse.getUsername(),
                measurerResponse.getAddress().getIp(), measurerResponse.getAddress().getPort());
    }


    public int getParametar() {
        return parametar;
    }

    public void setParametar(int parametar) {
        this.parametar = parametar;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Measurer measurer = (Measurer) o;
        return Objects.equals(username, measurer.username);
    }

    @Override
    public int hashCode() {
        return Objects.hash(username);
    }

    @Override
    public String toString() {
        return "Measurer{" +
                "id='" + id + '\'' +
                ", username='" + username + '\'' +
                ", address='" + address + '\'' +
                ", port=" + port +
                ", parametar=" + parametar +
                '}';
    }
}
