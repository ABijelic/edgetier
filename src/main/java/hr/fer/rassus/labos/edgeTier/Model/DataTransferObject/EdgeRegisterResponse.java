package hr.fer.rassus.labos.edgeTier.Model.DataTransferObject;


public class EdgeRegisterResponse {

    private String username;
    private Boolean success;
    private String message;
    private String mgrs;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMgrs() {
        return mgrs;
    }

    public void setMgrs(String mgrs) {
        this.mgrs = mgrs;
    }

    @Override
    public String toString() {
        return "EdgeRegisterResponse{" +
                "username='" + username + '\'' +
                ", success=" + success +
                ", message='" + message + '\'' +
                ", mgrs='" + mgrs + '\'' +
                '}';
    }
}
