package hr.fer.rassus.labos.edgeTier.Service;

import com.sun.research.ws.wadl.HTTPMethods;
import hr.fer.rassus.labos.edgeTier.Model.DataTransferObject.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.client.methods.HttpHead;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
public class RepositoryCommunicationService {

    @Value("${central-repository.url}")
    private String centralServerAddress;

    @Value("${central-repository.port}")
    private String port;

    @Value("${server.latitude}")
    private double latitude;

    @Value("${server.longitude}")
    private double longitude;

    private EdgeConfigurationService edgeConfigurationService;

    private MeasurementService measurementService;

    private MeasurerService measurerService;

    @Autowired
    public RepositoryCommunicationService(EdgeConfigurationService edgeConfigurationService,
                                          MeasurementService measurementService,
                                          MeasurerService measurerService) {
        this.edgeConfigurationService = edgeConfigurationService;
        this.measurementService = measurementService;
        this.measurerService = measurerService;
    }

    /**
     * @param edgeRegisterRequest DTO object for registration
     * @return boolean for success
     */
    public boolean register(EdgeRegisterRequest edgeRegisterRequest) {
        String address = "http://" + centralServerAddress + ":" + port;

        this.edgeConfigurationService.setAddress(edgeRegisterRequest.getIp() + ":" + edgeRegisterRequest.getPort());
        this.edgeConfigurationService.setLatitude(latitude);
        this.edgeConfigurationService.setLongitude(longitude);

        log.info("longitude = {}, latitude= {}", longitude, latitude);
        edgeRegisterRequest.setLatitude(latitude);
        edgeRegisterRequest.setLongitude(longitude);

        ClientHttpRequestFactory requestFactory = getClientHttpRequestFactory();

        RestTemplate restTemplate = new RestTemplate(requestFactory);

        HttpEntity<EdgeRegisterRequest> request = new HttpEntity<>(edgeRegisterRequest);

        try {
            EdgeRegisterResponse response = restTemplate.postForObject(address + "/api/edge/register", request,
                    EdgeRegisterResponse.class);

            System.out.println(response);

            this.edgeConfigurationService.setMgrs(response.getMgrs());
            this.edgeConfigurationService.setUsername(response.getUsername());

            getAllMeasurers(address);

            return response.getSuccess();
        } catch (ResourceAccessException e) {
            return false;
        }

    }

    public void logout() {
        String address = "http://" + centralServerAddress + ":" + port + "/api/edge/unregister";

        log.info("send on {} request for logout", address);

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("username", edgeConfigurationService.getUsername());

        HttpEntity httpEntity = new HttpEntity("", httpHeaders);
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.exchange(address, HttpMethod.PUT, httpEntity, Void.class);
    }

    public void sendMeasurements() {

        ObjectMapper mapper = new ObjectMapper();
        if (measurementService.isEmpty()) {
            System.out.println("no measurement");
            return;
        }
        final String uri = "http://" + centralServerAddress + ":" + port + "/api/measurement";
        final HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("username", edgeConfigurationService.getUsername());

        MeasurementsForCentralRepository list = measurementService.getAndClearAllMeasurement();
        try {
            log.info("send json {} to central repository {} ", mapper.writeValueAsString(list), uri);
        } catch (IOException e) {
            log.warn("can't parse to json list of measurements");
        }

        HttpEntity<Object> httpEntity = new HttpEntity<>(measurementService.getAndClearAllMeasurement(), httpHeaders);
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.exchange(uri, HttpMethod.POST, httpEntity, Void.class);
    }


    private void getAllMeasurers(String address) {
        String uri = address + "api/measurer";
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("username", edgeConfigurationService.getUsername());

        HttpEntity httpEntity = new HttpEntity("", httpHeaders);
        RestTemplate restTemplate = new RestTemplate();
        Object response = restTemplate.exchange(uri, HttpMethod.GET, httpEntity, List.class);
        if (response instanceof List) {
            this.measurerService.addMeasurers(((List<MeasurerResponse>) response)
                    .stream().map(measurer -> measurer.toMeasurer())
                    .collect(Collectors.toList()));
        } else {
            System.out.println("no measurers in the area");
        }


    }

    private ClientHttpRequestFactory getClientHttpRequestFactory() {
        HttpComponentsClientHttpRequestFactory clientHttpRequestFactory = new HttpComponentsClientHttpRequestFactory();
        clientHttpRequestFactory.setConnectTimeout(10000);
        return clientHttpRequestFactory;
    }


}
