package hr.fer.rassus.labos.edgeTier.Model.DataTransferObject;

import org.apache.hadoop.util.bloom.BloomFilter;

import java.io.*;

public class BloomFilterDTO {
    private int vectorSize;
    private int nmHash;
    private byte[] data;

    public BloomFilterDTO() {
    }

    public int getVectorSize() {
        return vectorSize;
    }

    public void setVectorSize(int vectorSize) {
        this.vectorSize = vectorSize;
    }

    public int getNmHash() {
        return nmHash;
    }

    public void setNmHash(int nmHash) {
        this.nmHash = nmHash;
    }

    public byte[] getData() {
        return data;
    }

    public void setData(byte[] data) {
        this.data = data;
    }


    public static BloomFilterDTO fromBloomFileter(BloomFilter bloomFilter, int nmHash) {
        BloomFilterDTO bloomFilterDTO = new BloomFilterDTO();
        bloomFilterDTO.setVectorSize(bloomFilter.getVectorSize());
        bloomFilterDTO.setNmHash(nmHash);
        byte[] data = new byte[bloomFilter.getVectorSize()];
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        DataOutput dataOutput = new DataOutputStream(byteArrayOutputStream);
        try {
            bloomFilter.write(dataOutput);
            data = byteArrayOutputStream.toByteArray();
            System.out.println("bloom-filter: "+data.toString());
            bloomFilterDTO.setData(data);
            ((DataOutputStream) dataOutput).close();
            return bloomFilterDTO;
        } catch (IOException e) {
            return null;
        }
    }
}
