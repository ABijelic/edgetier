package hr.fer.rassus.labos.edgeTier;

import hr.fer.rassus.labos.edgeTier.Model.DataTransferObject.EdgeRegisterRequest;
import hr.fer.rassus.labos.edgeTier.Model.DataTransferObject.ParametarResponse;
import hr.fer.rassus.labos.edgeTier.Model.Measurer;
import hr.fer.rassus.labos.edgeTier.Service.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import javax.annotation.PreDestroy;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.List;

@Slf4j
@SpringBootApplication
@EnableScheduling
public class EdgeTierApplication {
    public static final double FALSE_POS_RATE = 0.01;
    public static final boolean BLOOM_FILTER = true;
    public static final String MESSAGE_GET_PARAMETAR_FROM_CLIENT = "sendParameters";
    public static final String MESSAGE_GET_MEASUREMENT_TOP_K = "startMeasuringTopK";
    public static final int NUMBER_K = 5;
    public static final String MESSAGE_USE_BLOOM_FILTER = "startMeasuringBloom";
    public static final String KILL = "Exit!";

    private boolean registered = false;
    private static ConfigurableApplicationContext ctx;

    @Autowired
    public MeasurerService measurerService;

    @Autowired
    public EdgeConfigurationService edgeConfigurationService;

    @Autowired
    public RepositoryCommunicationService repositoryCommunicationService;

    @Autowired
    public BloomFilterService bloomFilterService;

    @Autowired
    public CommunicationService communicationService;


    public static void main(String[] args) {

        ctx = SpringApplication.run(EdgeTierApplication.class, args);
    }

    @EventListener
    public void EventListenerExecute(ApplicationReadyEvent event) {
        int port = (Integer) event.getApplicationContext().getEnvironment().getPropertySources().get("server.ports").getProperty("local.server.port");
        EdgeRegisterRequest registerRequest = null;
        try {
            registerRequest = new EdgeRegisterRequest(port);
        } catch (SocketException | UnknownHostException e) {
            e.printStackTrace();
        }

        log.info("registration....");
        while (!repositoryCommunicationService.register(registerRequest)) {
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                log.warn("Interruption happen. ");
            }
            log.info("repeat: registration....");
        }
        log.info("registred");
        registered = true;
        communicateWithMeasurer();
    }

    //@Sheduled(cron = "0 0/15 * * * *)
    @Scheduled(cron = "0/30 * * 1/1 * *")
    public void communicateWithMeasurer() {
        List<Measurer> measurers = measurerService.getAllMeasurers();
        log.info("refreshing. NUmber of measurer is {}", measurers.size());
        if (!registered) {
            System.out.println("not registered");
            return;
        }
        if (BLOOM_FILTER) {
            bloomFilterService.initializBloomFilters();
            measurerService.sendOnAllMeasurement(MESSAGE_USE_BLOOM_FILTER);
        } else {
            measurerService.sendRequestsForParametar(MESSAGE_GET_PARAMETAR_FROM_CLIENT);
            measurerService.calculateTopK(NUMBER_K);
            measurerService.sendMessageOnTopKMeasurement(MESSAGE_GET_MEASUREMENT_TOP_K);
        }


    }


    //@Scheduled(cron = "0 0/2 * 1/1 * *")
    @Scheduled(cron = "0/20 * * * * *")
    public void sendMeasurements() {
        repositoryCommunicationService.sendMeasurements();
    }

    @Scheduled(fixedDelay = 1000, initialDelay = 5*60*1000)
    public void terminate() {
        ctx.close();
    }

    @PreDestroy
    public void destroy() {
        repositoryCommunicationService.logout();
        measurerService.sendOnAllMeasurement(KILL);
        communicationService.closeWriter();
        log.warn("I died :/");
    }
}

