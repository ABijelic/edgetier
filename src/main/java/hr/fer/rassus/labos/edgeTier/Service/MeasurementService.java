package hr.fer.rassus.labos.edgeTier.Service;

import hr.fer.rassus.labos.edgeTier.EdgeTierApplication;
import hr.fer.rassus.labos.edgeTier.Model.DataTransferObject.MeasurementForCentralRepository;
import hr.fer.rassus.labos.edgeTier.Model.DataTransferObject.MeasurementsForCentralRepository;
import hr.fer.rassus.labos.edgeTier.Model.Measurement;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
public class MeasurementService {
    private List<Measurement> measurements;

    private BloomFilterService bloomFilterService;

    @Autowired
    public MeasurementService(BloomFilterService bloomFilterService) {
        this.bloomFilterService = bloomFilterService;
        this.measurements = new ArrayList<>();
    }

    public boolean addMeasurement(Measurement measurement) {
        log.info("add measurement {} on measurement service", measurement);
        if (EdgeTierApplication.BLOOM_FILTER) {
            if(bloomFilterService.addMeasurement(measurement)) {
                log.info("measurement is pass bloom filter");
                return measurements.add(measurement);
            } else {
                log.info("measurement isn't pass bloom filter");
                return false;
            }
        }
        return measurements.add(measurement);
    }

    public List<Measurement> getAllMeasurement() {
        return measurements;
    }

    public MeasurementsForCentralRepository getAndClearAllMeasurement() {
        List<MeasurementForCentralRepository> result = measurements.stream().map(measurement -> measurement.toMeasurementForCentralRepository()).collect(Collectors.toList());
        measurements.clear();
        return new MeasurementsForCentralRepository(result);
    }

    public boolean isEmpty() {
        return measurements.size() == 0;
    }

    public void clearMeasurements() {
        measurements.clear();
    }


}
